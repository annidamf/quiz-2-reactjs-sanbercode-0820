import React, {Component} from React;

class Name extends Component{
  render(){
  return <p>{this.props.name}</p>;
}};

class Gender extends Component{
  render(){
  return <p>{this.props.gender}</p>;
}
};

class Profession extends Component{
  render(){
  return <p>{this.props.profession}</p>;
}
};

class Photo extends Component{
  render(){
  return <p>{this.props.photo}</p>;
}
};



const data =
[{name: "John", age: 25, gender: "Male", profession: "Engineer", photo: "https://media.istockphoto.com/photos/portarit-of-a-handsome-older-man-sitting-on-a-sofa-picture-id1210237745"}, 
{name: "Sarah", age: 22, gender: "Female", profession: "Designer", photo: "https://cdn.pixabay.com/photo/2018/01/15/07/51/woman-3083378_960_720.jpg"}, 
{name: "Sarah", age: 22, gender: "Female", profession: "Designer", photo: "https://cdn.pixabay.com/photo/2018/01/15/07/51/woman-3083378_960_720.jpg"}, 
{name: "Kate", age: 27, gender: "Female", profession: "Model", photo: "https://cdn.pixabay.com/photo/2015/05/17/20/07/fashion-771505_960_720.jpg" }];

class Card extends Component{
  render(){
    return(
      <>
        data.map(item=> {
        return(
        <div class="blog-card">
      <div class="blog-card-thumbnail">
        <img src="{item.photo}">
      </div>

      <div class="blog-card-details">
        <div class="blog-card-title">
          {item.name} </br> {item.profession}
        </div>
        </div>
      </>)}
      
      )
}
}